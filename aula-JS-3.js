// Criar uma função que, dado um número indefinido de arrays,
//     faça a soma de seus valores
//     Ex: [1,2,3] [1,2,2] [1,1] => 13
//         [1,1] [2, 20] => 24

function somaArrays(...arrays) {
    let soma = 0;

    for (i in arrays){
        currArr = arrays[i]
        for (j in currArr) {
            soma += currArr[j]
        }
    }
    console.log(soma)
}

let arrayA = [2, 2]
let arrayB = [3, 4]
let arrayC = [5, 6, 1, 2, 6]

somaArrays(arrayA, arrayB, arrayC);

// Criar uma função que dado um número n e um array, retorne
//     um novo array com os valores do array anterior * n
//     Ex: (2, [1,3,6,10]) => [2,6,12,20]
//         (3, [7,9,11,-2]) => [21, 27, 33, -6]

function multiplicaArray (num, array) {
    let novoArray = array.map((n) => {
        return n * num
    })
 //   return novoArray;
    console.log(novoArray)
}

let arr = [1, 2, 3]
multiplicaArray(3, arr);

/* Crie uma função que dado uma string A e um array de strings,
retorne um array novo com apenas as strings do array que são
compostas exclusivamente por caracteres da string A

Ex: ("ab", ["abc", "ba", "ab", "bb", "kb"]) => ["ba", "ab", "bb"]
    ("pkt", ["pkt", "pp", "pata", "po", "kkkkkkk"]) => ["pkt", pp, kkkkkkk]
     */
function findString(str, array) {
    let novoArray = array.filter((e) => {
        let ok = true

        for (i in e) {
            if ( !(str.includes(e[i])) ){
                ok = false
            }
        
        }
        if(ok){
            return e
        }
    })
    return novoArray
}

let arrayStr = ["pp", "pa", "kkk", "cd", "pad"]
let string = "pa"
console.log(findString(string, arrayStr))

/* Criar uma função que dado n arrays, retorne um novo array que possua
apenas os valores que existem em todos os n arrays
Ex: [1, 2, 3] [3, 3, 7] [9, 111, 3] => [3]
    [120, 120, 110, 2] [110, 2, 130] => [110, 2] */

/* Crie uma função que dado n arrays, retorne apenas os que tenham a
soma de seus elementos par
Ex: [1, 1, 3] [1, 2, 2, 2, 3] [2] => [1, 2, 2, 2, 3] [2]
        [2,2,2,1] [3, 2, 1] => [3,2,1] 
 */

function findSomaPar(...arrays) {

    for (i in arrays){
        let currArr = arrays[i]
       // console.log(currArr)
        let res = currArr.reduce((accum, curr) => accum + curr);
        if (res % 2 === 0){
            console.log(currArr)
        } 
    } 
    
}

findSomaPar(arrayA, arrayB, arrayC);